import type { NextPage } from 'next'
import Head from 'next/head'
import { useEffect, useState } from 'react'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {
  const [row, setRow] = useState("")
  const [column, setColumn] = useState("")

  const [rowArray, setRowArray] = useState<Array<number>>([]);
  const [colArray, setColArray] = useState<Array<number>>([]);

  const [showTable, setShowTable] = useState(false);

  const CreateTable = async () => {
    setShowTable(false);
    const rows: Array<number> = []
    const cols: Array<number> = []
    colArray.length = 0;

    for (let i = 1; i <= Number(row); i++) {
      rows.push(i);
    }
    setRowArray(rows);

    for (let i = 1; i <= Number(column); i++) {
      cols.push(i);
    }
    setColArray(cols);

    setShowTable(true);
  };

  useEffect(():void => {
    setRowArray([])
    setColArray([])
  }, [row, column])

  return (
    <div className={styles.container}>
      <Head>
        <title>Dynamic UI Table</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Dynamic UI Table
        </h1>

        <p className={styles.description}>
          Please fill Column count and Row count.
        </p>

        <div className={styles.grid} style={{minWidth: "35%", justifyContent: "space-between"}}>
          <div>
            <h2>Row</h2>
            <input type={"number"} value={row} onChange={(e) => setRow(e.target.value)}/>
          </div>
          <div>
            <h2>Column</h2>
            <input type={"number"} value={column} onChange={(e) => setColumn(e.target.value)}/>
          </div>
          <div style={{display: "flex", justifyContent: "flex-end"}}>
            <button onClick={CreateTable} disabled={Number(row) <= 0 || Number(column) <= 0}>Create Table</button>
          </div>
        </div>
        <div style={{ padding: "2rem 0" }}>
          {showTable ? (
            <table style={{ border: "1px solid black", borderCollapse: "collapse" }}>
              <tbody>
                {rowArray.map((r, index) => (
                  <tr key={`row-${index}`}>
                    {colArray.map((c, index) => {
                      const biggestSide = row > column ? row : column
                      const rightSide = Number(biggestSide) + (row === column || column > row ? 1 : 0)
                      return (
                        <td
                          key={index}
                          style={{
                            position: "relative",
                            border: "1px solid black",
                            borderCollapse: "collapse",
                            color: c === r || c + r === rightSide ? "black" : "white"
                          }}
                        >
                          <div style={{ padding: "1rem" }}>
                            {c === r || c + r === rightSide ? (<div className={styles.triangle} />) : null}
                            {`${r-1},${c-1}`}
                          </div>
                        </td>
                      )
                    })}
                  </tr>
                ))}
              </tbody>
            </table>
          ) : null}
        </div>
      </main>
    </div>
  )
}

export default Home
